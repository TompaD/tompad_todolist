# tompad_ToDoList

tompad_ToDOList is a reascript to save notes on what to do in a Reaperproject.


You can create new, edit, mark done, delete and set priority of the different ToDo-items.


The settings and ToDos is saved in the project folder as a file -  **todolist.txt.**

The way to set priority on ToDos is by rightclicking the item and choose from dropdown menu.

- |H| = High priority

- |M| = Medium priority

- |L| = Low priority

The ToDos in list is sorted in order High - Medium - Low - Done

To mark a ToDo done click button Done and priority changes to |X| (= Done).

___

Any comments on coding, requests, bugs etc is welcome! PM me (tompad) on Reaper Forum (https://forum.cockos.com/member.php?u=19103).

This script wouldn't exist without the help from the Reaper community:

- Lokasenna
- XRayM
- cfillion
- mespotine

... and many more, **thanks to you all!**
